/*********************************WebUpload 单文件上传 begin*****************************************/
$(function(){
  var $list = $("#thelist");
  var subimt_path = $('#upForm').attr('action');
    var  uploader ;// 实例化
  uploader = WebUploader.create({
         // auto:false, //是否自动上传
          pick: {
              id: '#attach',
             // name:"file",  //这个地方 name 没什么用，虽然打开调试器，input的名字确实改过来了。但是提交到后台取不到文件。如果想自定义file的name属性，还是要和fileVal 配合使用。
              label: '点击选择视屏',
              multiple:false            //默认为true，就是可以多选
          },
          chunkRetry: false,
          swf: 'Uploader.swf',
          //server: "/fileUploadSave",
		  server:subimt_path,
          duplicate:true,//是否可重复选择同一文件
          resize: false,
          /*formData: {
              "status":"file",
              "contentsDto.contentsId":"0000004730",
              "uploadNum":"0000004730",
              "existFlg":'false'
          },*/
      // 只允许选择视频文件文件
      accept: {
          title: 'Videos',
          extensions: 'mp4,3gp,mov,avi,wmv,flv,m3u8,ts,rm,rmvb,mpg,m4v',
          mimeTypes: 'video/*'
      },
         //compress: null,//图片不压缩
         fileNumLimit: 1,
         fileSizeLimit: 50 * 1024 * 1024,
         fileSingleSizeLimit: 50 * 1024 * 1024,
        //  chunked: false,  //分片处理
         // chunkSize: 5 * 1024 * 1024, //每片5M
       //   chunkRetry:false,//如果失败，则不重试
          //threads:1,//上传并发数。允许同时最大上传进程数。
          // runtimeOrder: 'flash',  
          // 禁掉全局的拖拽功能。这样不会出现图片拖进页面的时候，把图片打开。  
          disableGlobalDnd: true
      });  
     
    // 当有文件添加进来的时候
     uploader.on( "fileQueued", function( file ) {
       console.log("fileQueued:");
          $list.append( "<div id='"+  file.id + "' class='item'> </div>" );
         document.getElementById('upFile').files[0] =file;
         var f2 = document.getElementById('upFile').files[0];
         if (f2) {
             var fileSize = 0;
             if (f2.size > 1024 * 1024)
                 fileSize = (Math.round(f2.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
             else
                 fileSize = (Math.round(f2.size * 100 / 1024) / 100).toString() + 'KB';
             $("#fileSize").val(fileSize);
             $("#fileInfo").text('文件名称 : ' + f2.name+ '    文件大小: ' + fileSize) ;
             //document.getElementById('fileTypes').innerHTML = 'Type: ' + f2.type;

         }
         $list.append( "<div  >" +
             "<img  id='imageTig' src='/statics/photopay/upload/ok.png'> </img>" +
           /*  "<p class='state'>等待上传...</p>" +*/
             "</div>" );
         $("#attach").hide();
     });
     
     //当某个文件的分块在发送前触发
     uploader.on("uploadBeforeSend",function(object ,data){
       console.log("uploadBeforeSend");
       console.log(object);
       console.log(data);
     })

    // 只能上传一个视频文件
    uploader.on('error', function (file) {
        $.alert(file);
        if (file == "Q_EXCEED_NUM_LIMIT") {

            $.alert('您已经设置过上传的文件' );
            return false;
        } else if (file == "Q_TYPE_DENIED") {

                $.alert('不支持这个视频格式');
            return false;
        }
        if (file == 'Q_EXCEED_SIZE_LIMIT') {
            $.alert('不能上传超过50M的视频');
            return false;
        }
    });
     //当所有文件上传结束时触发
     uploader.on("uploadFinished",function(){
       console.log("uploadFinished:");
     })
     

     
     //当文件上传成功时触发。
       uploader.on( "uploadSuccess", function( file, response) {
           $.hideLoading();
           //$.alert(response.shortUrl);
          /* $.prompt({
               title: "访问链接",
               input:response.shortUrl,
               //empty: false, // 是否允许为空
               onOK: function (input) {
                   //点击确认
               },
               onCancel: function () {
                   //点击取消
               }
           });*/
          /* $.modal({
               title: "访问链接",
               text: "我是自定义的modal",
               input:response.shortUrl,
               buttons: [
                   { text: "取消", className: "default", onClick: function(){ console.log(3)} }
               ]
           });*/
          if(response.tag =="0"){
             // $.alert("连接生成失败！！");
              $.modal({
                  title: "连接生成失败！！",
                  buttons: [
                      // { text: "编辑", onClick: function(){ console.log(1)} },
                      { text: "关闭", className: "default", onClick: function(){  window.location.reload(); console.log(3)} }
                  ]
              });
              //window.location.reload();
          }else{
              $.modal({
                  title: "链接地址",
                  text:"<input id='foo' type='text' value='"+response.shortUrl+"' > <button class='btn' data-clipboard-action='copy' data-clipboard-target='#foo'>复制链接</button>" ,

                  buttons: [
                      // { text: "编辑", onClick: function(){ console.log(1)} },
                      { text: "关闭", className: "default", onClick: function(){  window.location.reload(); console.log(3)} }
                  ]
              });
          }

          /* $.alert("自定义的消息内容", function() {
               //点击确认后的回调函数
           });*/
     });

     uploader.on( "uploadError", function( file,reason ) {
         $.hideLoading();
         $.toptip('操作失败', 'error');
         $.alert("上传出错");
         $("#imageTig").attr("src","../images/no.jpg");
         //$( "#"+file.id ).find("p.state").text("上传出错");
         //uploader.cancelFile(file);
        // uploader.removeFile(file,true);
         //uploader.reset();
     });

    $("#upload").click(function () {
        //uploader.options.formData.title = "text";

        if($('#fileSize').val().length ==0 ){
            $.alert("请先选择上传的视频");
            return;
        }
        if($('#title').val().length ==0 ){
            $.alert("请先输入标题");
            return;
        }
        if($('#fileMaxje').val().length ==0 ){
            $.alert("请先输入积分");
            return;
        }
        if($('#fileMaxje').val() < 1 || $('#fileMaxje').val() >50){
            $.alert("积分范围只能（2-50）");
            return;
        }
        $.showLoading("上传中。。。");
        //alert($("input[name='fileMs']").val());
        uploader.option('formData',{
            openId:$('#openId').val(),
            fileMs:$("input[name='fileMs']").val(),
            fileMh:$("#sliderValue").text(),
            fileMaxje:$('#fileMaxje').val(),
            userId:$('#userId').val(),
            title:$('#title').val()
        })
        uploader.upload();
        //$.hideLoading();
    })

//文件上传进度 文件上传中，Web Uploader会对外派送uploadProgress事件，其中包含文件对象和该文件当前上传进度。
    // 文件上传过程中创建进度条实时显示。
    uploader.on('uploadProgress', function (file, percentage) {
        var $li = $('#' + file.id),
            $percent = $li.find('.barContainer .bar');

        // 避免重复创建
        if (!$percent.length) {
            $percent = $('<div class="barContainer"  style="width: 100%;text-align: center"><div class="bar" role="progressbar" style="width: 100%"></div></div>').appendTo($li).find('.bar');

        }

       // $li.find('p.state').text('上传中');
        $percent.css( 'width', percentage * 100 + '%' );
       $percent.html((percentage * 100).toFixed(2) + '%');
    });
 // 文件上传过程中创建进度条实时显示。
   /* uploader.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.barContainer .bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.bar');
        }

       // $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });*/
});
function inputNumberF(dom, style) {
    if (style == 1) {
        // 只能输入一个小数点
        dom.value = dom.value.replace(/[^\d.]/g, "").replace(/^\./g, "").replace(/\.{2,}/g, ".").replace(".", "$#$").replace(/\./g, "").replace("$#$", ".").replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3');
        if (dom.value) {
            if (dom.value > 50) {
                dom.value = 50;
                //$('#fileMaxje').html(1000);
            } else if (dom.value < 1) {
                dom.value = 1;
               // $('#fileMaxje').html(1);
            } else {
              //  dom.value = 1;
               // $('#fileMaxje').html(dom.value);
            }
        } else {
            //dom.value = 1;
            //                                      dom.value = 0;
           // $('#fileMaxje').html(0);
        }
    }

}
/*********************************WebUpload 单文件上传 end*******************************************/