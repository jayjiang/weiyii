<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WxApps */

$this->title = '添加微信';
$this->params['breadcrumbs'][] = ['label' => '微信管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wx-apps-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
