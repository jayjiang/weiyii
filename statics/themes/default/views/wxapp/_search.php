<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\WxAppsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wx-apps-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'public_id') ?>

    <?= $form->field($model, 'public_name') ?>

    <?= $form->field($model, 'wechat') ?>

    <?php // echo $form->field($model, 'headface_url') ?>

    <?php // echo $form->field($model, 'token') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'appid') ?>

    <?php // echo $form->field($model, 'secret') ?>

    <?php // echo $form->field($model, 'encodingasekey') ?>

    <?php // echo $form->field($model, 'check_file') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
