<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\WxAppsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '微信管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wx-apps-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('添加微信', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'public_id',
            'public_name',
            'wechat',
            // 'headface_url:url',
            // 'token',
            // 'type',
            // 'appid',
            // 'secret',
            // 'encodingasekey',
            // 'check_file',

            [
                'class' => 'common\grid\ActionColumn',
                'header' => '操作',
                'template' => '{manage}{update} {view} {delete} ',
			    'buttons' => [
				  'manage' => function ($url,$model,$key){
					  $options = [
						   'title' => '进入管理',
						   'aria-label' => '进入管理',
						   'class' => 'btn btn-success btn-xs',
						   'data-pjax' => '0',
					   ];
					  return Html::a('<span class="fa fa-edit"></span> 进入管理 ', $url, $options);
				  }
				],
            ],
        ],
    ]); ?>
</div>
