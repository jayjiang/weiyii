<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WxApps */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wx-apps-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'public_id')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'public_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'wechat')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'token')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->dropDownList([]) ?>
    <?= $form->field($model, 'appid')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'secret')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'encodingasekey')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'check_file')->fileInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
