<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WxApps */

$this->title = '编辑: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => '微信管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '编辑';
?>
<div class="wx-apps-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
