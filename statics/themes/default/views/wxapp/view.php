<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\WxApps */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wx Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wx-apps-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['编辑', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['删除', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            
           // 'user_id',
            'public_id',
            'public_name',
            'wechat',
           // 'headface_url:url',
            [
                'label'=>'服务器地址(URL)',
                'value'=>Yii::$app->request->hostInfo."wx/valid/".$model->id,
            ],
            'token',
            //'type',
            'appid',
            'secret',
            'encodingasekey',
            'check_file',
        ],
    ]) ?>

</div>
