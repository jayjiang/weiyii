<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "t_photo_video".
 *
 * @property string $id
 * @property string $wxid
 * @property string $opneid
 * @property string $video_img
 * @property string $video_src
 * @property integer $status
 * @property string $title
 * @property string $info
 * @property integer $created_at
 * @property integer $updated_at
 */
class PhotoVideo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_photo_video';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['wxid'], 'string', 'max' => 100],
            [['opneid', 'video_img', 'video_src', 'title', 'info'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wxid' => 'Wxid',
            'opneid' => 'Opneid',
            'video_img' => 'Video Img',
            'video_src' => 'Video Src',
            'status' => 'Status',
            'title' => 'Title',
            'info' => 'Info',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
