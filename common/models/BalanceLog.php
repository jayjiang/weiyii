<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_balance_log".
 *
 * @property integer $id
 * @property string $opneid
 * @property integer $type
 * @property string $original
 * @property integer $operators
 * @property string $pice
 * @property string $results
 * @property string $info
 * @property integer $created_at
 * @property integer $updated_at
 */
class BalanceLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_balance_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'operators', 'created_at', 'updated_at'], 'integer'],
            [['original', 'pice', 'results'], 'number'],
            [['created_at', 'updated_at'], 'required'],
            [['opneid'], 'string', 'max' => 200],
            [['info'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'opneid' => 'Opneid',
            'type' => 'Type',
            'original' => 'Original',
            'operators' => 'Operators',
            'pice' => 'Pice',
            'results' => 'Results',
            'info' => 'Info',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
