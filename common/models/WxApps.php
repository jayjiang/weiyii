<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "t_wx_apps".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $public_id
 * @property string $public_name
 * @property string $wechat
 * @property string $headface_url
 * @property string $token
 * @property integer $type
 * @property string $appid
 * @property string $secret
 * @property string $encodingasekey
 * @property string $check_file
 * @property string $mchid
 * @property string $mchkey
 * @property string $cert_pem
 * @property string $key_pem
 */
class WxApps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_wx_apps';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'integer'],
            [['public_id', 'public_name', 'wechat', 'headface_url', 'appid', 'secret', 'encodingasekey', 'mchid', 'mchkey', 'cert_pem', 'key_pem'], 'string', 'max' => 200],
            [['token'], 'string', 'max' => 50],
            [['check_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'public_id' => '公众号原始ID',
            'public_name' => '公众号名称',
            'wechat' => '微信号',
            'headface_url' => 'LOGO',
            'token' => 'Token',
            'type' => 'Type',
            'appid' => 'Appid',
            'secret' => 'Secret',
            'encodingasekey' => 'Encodingasekey',
            'check_file' => 'Check File',
            'mchid' => '商户号',
            'mchkey' => 'API密钥',
            'cert_pem' => '微信支付证书CERT',
            'key_pem' => '微信支付证书KEY',
        ];
    }
}
