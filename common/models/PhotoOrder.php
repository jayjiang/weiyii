<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "t_photo_order".
 *
 * @property string $id
 * @property string $wxid
 * @property integer $video_id
 * @property string $opneid
 * @property string $money
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class PhotoOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_photo_order';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['money'], 'number'],
            [['wxid','order_no'], 'string', 'max' => 100],
            [['opneid'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wxid' => 'Wxid',
            'video_id' => 'Video ID',
            'order_no' => '订单号',
            'opneid' => 'Opneid',
            'money' => '金额',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
