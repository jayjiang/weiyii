<?php
namespace  common\libs\wxapi;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Yii;
use yii\debug\models\search\Log;
/**
 * Description of Api
 *
 * @author SFB
 */
class WechatApi {
  private $confToken = "";
  private $appid;
  private $appsecret;
  
  public function setAppsecret($secret){
      $this->appsecret = $secret;
  }
  public function getAppsecret(){
     return  $this->appsecret;
  }
  public function setAppid($appid){
      $this->appid = $appid;
  }
  
  public function getAppid(){
      return $this->appid;
  }
  
  public function setConfToken($token){
      $this->confToken = $token;
  }
  
  public function getConfToken(){
      return $this->confToken;
  }
 
  
  //参数校验
  private function checkSignature($signature,$timestamp,$nonce,$token=null){
          if(empty($token)){
              $token = $this->getConfToken();
          }
        if (!$token) {
            echo 'TOKEN is not defined!';
        }else{
            $tmpArr = array($token, $timestamp, $nonce);
            // use SORT_STRING rule
            sort($tmpArr, SORT_STRING);
            $tmpStr = implode( $tmpArr );
            $tmpStr = sha1( $tmpStr );
            if( $tmpStr == $signature ){
                return true;
            }else{
                return false;
            }
        }
    }
    
    //微信服务接入时，服务器需授权验证
    public function valid($token=null)
    {
        if(empty($token)){
            $token = $this->getConfToken();
        }
        $echoStr = Yii::$app->request->get("echostr");
        $signature = Yii::$app->request->get("signature");
        $timestamp = Yii::$app->request->get("timestamp");
        $nonce = Yii::$app->request->get('nonce');
        //valid signature , option
        Yii::info("echostr={$echoStr} signature={$signature} timestamp={$timestamp} nonce={$nonce} token={$token} \n");
        if($this->checkSignature($signature,$timestamp,$nonce,$token)){
            echo $echoStr;
        }
    }
    
    //用户授权接口：获取access_token、openId等；获取并保存用户资料到数据库
    public function accesstoken()
    {
        $code = $_GET["code"];
        $state = $_GET["state"];
        //$appid = Yii::$app->params['wechat']['appid'];
        $appid= $this->appid;
        //$appsecret = Yii::$app->params['wechat']['appsecret'];
        $appsecret= $this->appsecret;
        $request_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$appsecret.'&code='.$code.'&grant_type=authorization_code';
        //初始化一个curl会话
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = $this->response($result);
        //获取token和openid成功，数据解析
        $access_token = $result['access_token'];
        $refresh_token = $result['refresh_token'];
        $openid = $result['openid'];
        //请求微信接口，获取用户信息
        $userInfo = $this->getUserInfo($access_token,$openid);
        $userInfo["openid"] = $openid;
        $userInfo['access_token'] = $access_token;
        $userInfo['refresh_token'] = $refresh_token;
        return $userInfo;
    }
    
    //从微信获取用户资料
    public function getUserInfo($access_token,$openid)
    {
        $request_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
        //初始化一个curl会话
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = $this->response($result);
        return $result;
    }
    
    private function response($text)
    {
        return json_decode($text, true);
    }
    
    
}
