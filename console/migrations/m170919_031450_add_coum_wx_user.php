<?php

use yii\db\Migration;

class m170919_031450_add_coum_wx_user extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE  `t_wechat_user`"
			. " ADD `wallet` DOUBLE(10,2) NOT NULL DEFAULT 0 COMMENT '包钱金额' ";
			//. " ADD  `trade_status` char(50) NOT NULL DEFAULT '' COMMENT '交易状态'";
      #  $this->execute($sql);
		
		$sql ="CREATE TABLE `t_balance_log` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`opneid` char(200) NOT NULL DEFAULT '' COMMENT 'OPENID',
			`type` int(11) NOT NULL DEFAULT '0' COMMENT '日志类型',
			`original` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '原来的金额',
			`operators` smallint(4) NOT NULL DEFAULT '0' COMMENT '操作符 加减乘除',
			`pice` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '要加减乘除 的金额',
			`results` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '加减乘除之后的金额',
			`info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '需求描述',
			`created_at` int(11) NOT NULL,
			`updated_at` int(11) NOT NULL,
			PRIMARY KEY (`id`)
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ";
		$this->execute($sql);
    }

    public function safeDown()
    {
       $this->execute("drop table `m_balance_log`");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_031450_add_coum_wx_user cannot be reverted.\n";

        return false;
    }
    */
}
