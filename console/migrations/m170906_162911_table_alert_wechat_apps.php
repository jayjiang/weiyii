<?php

use yii\db\Migration;

class m170906_162911_table_alert_wechat_apps extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `t_wx_apps` ADD COLUMN `mchid` CHAR(200) NOT NULL DEFAULT '' COMMENT '商户号',
        ADD COLUMN `mchkey` CHAR(200) NOT NULL DEFAULT '' COMMENT 'API密钥',
        ADD COLUMN `cert_pem` CHAR(200) NOT NULL DEFAULT '' COMMENT '微信支付证书cert',
        ADD COLUMN `key_pem` CHAR(200) NOT NULL DEFAULT '' COMMENT '微信支付证书key'";
        $this->execute($sql);
        
        $sql = " CREATE TABLE `t_photo_video`(
            `id` int(11) unsigned   AUTO_INCREMENT   COMMENT 'ID',
            `wxid` CHAR(100) NOT NULL DEFAULT '' COMMENT '微信ID',
            `opneid` CHAR(200) NOT NULL DEFAULT '' COMMENT 'OPENID',
            `video_img` CHAR(200) NOT NULL DEFAULT '' COMMENT '视频封面',
            `video_src` CHAR(200) NOT NULL DEFAULT '' COMMENT '视频图片',
            `status` tinyint(3)  NOT NULL DEFAULT 0 COMMENT '状态',
            `title` CHAR(200) NOT NULL DEFAULT '' COMMENT 'TITLE',
            `info` CHAR(200) NOT NULL DEFAULT '' COMMENT '描述',
            `created_at` INT(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
            `updated_at` INT(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $this->execute($sql);
        
    }

    public function safeDown()
    {
        $this->execute("dorp table `t_photo_video`");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170906_162911_table_alert_wechat_apps cannot be reverted.\n";

        return false;
    }
    */
}
