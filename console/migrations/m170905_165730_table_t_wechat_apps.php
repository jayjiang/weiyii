<?php

use yii\db\Migration;

class m170905_165730_table_t_wechat_apps extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `t_wx_apps`(
            `id` int(11) unsigned NOT NULL  DEFAULT 0 COMMENT 'ID',
            `user_id` int(11) unsigned NOT NULL  DEFAULT 0 COMMENT '用户ID',
            `public_id` char(200) NOT NULL DEFAULT '' COMMENT '公众号原始ID',
            `public_name` char(200) NOT NULL DEFAULT '' COMMENT '公众号名称',
            `wechat`  char(200) NOT NULL DEFAULT '' COMMENT '微信号',
            `headface_url` CHAR(200) NOT NULL DEFAULT '' COMMENT '公众号头像LOGO',
            `token`  CHAR(50) NOT NULL DEFAULT 'WEIYII' COMMENT 'token',
            `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '微信公众号类型',
            `appid` CHAR(200) NOT NULL DEFAULT '' COMMENT '公众号ID(APPID)',
            `secret` CHAR(200) NOT NULL DEFAULT '' COMMENT 'Secret',
            `encodingasekey` CHAR(200) NOT NULL DEFAULT '' COMMENT 'EncodingAESKey',
            `check_file`  varchar(255) NOT NULL DEFAULT '' COMMENT '域名授权验证文件',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $this->execute($sql);
        
        $sql1 = "CREATE TABLE `t_wechat_user` (
              `id` int(11) unsigned NOT NULL  DEFAULT 0 COMMENT 'ID',
              `wx_id` int(11) unsigned NOT NULL  DEFAULT 0 COMMENT '微信配置ID',
              `openid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `nickname` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '微信昵称',
              `sex` tinyint(4) NOT NULL COMMENT '性别',
              `headimgurl` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '头像',
              `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '国家',
              `province` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '省份',
              `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '城市',
              `access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `refresh_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
               PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $this->execute($sql1);
    }

    public function safeDown()
    {
        $rmSQl = "drop table `t_wx_apps`";
        $this->execute($rmSQl);
        
        $rmSQl1 ="drop table  `t_wechat_user`";
        $this->execute($rmSQl1);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170905_165730_table_t_wechat_apps cannot be reverted.\n";

        return false;
    }
    */
}
