<?php

use yii\db\Migration;

class m170908_043116_table_update_cumm extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE  `t_photo_order` ADD `trade_no` char(50) NOT NULL DEFAULT '' COMMENT '交易号',ADD  `trade_status` char(50) NOT NULL DEFAULT '' COMMENT '交易状态'";
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170908_043116_table_update_cumm cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170908_043116_table_update_cumm cannot be reverted.\n";

        return false;
    }
    */
}
