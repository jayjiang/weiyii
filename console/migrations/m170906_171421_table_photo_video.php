<?php

use yii\db\Migration;

class m170906_171421_table_photo_video extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `t_photo_order`(
            `id` int(11) unsigned   AUTO_INCREMENT   COMMENT 'ID',
            `order_no` char(50) NOT NULL DEFAULT '' COMMENT '订单号',
            `wxid` CHAR(100) NOT NULL DEFAULT '' COMMENT '微信ID',
            `video_id` INT(11) NOT NULL DEFAULT 0 COMMENT '视频ID',
            `opneid` CHAR(200) NOT NULL DEFAULT '' COMMENT 'OPENID',
            `money`  decimal(10,2) NOT NULL DEFAULT 0 COMMENT '打赏金额',
            `status` int(1) not null default 0 comment '状态',
            `created_at` int(10) not null default 0 comment '',
            `updated_at` int(10) not null default 0 comment '',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $this->execute($sql);
        
        $sql = "ALTER TABLE `t_photo_order` ADD INDEX index_name ( `opneid` ) ";
        $this->execute($sql);
        
        $sql = "ALTER TABLE `t_photo_video` ADD INDEX index_name ( `opneid` ) ";
        $this->execute($sql);
        
    }

    public function safeDown()
    {
        $this->execute("dorp table `t_photo_order`");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170906_171421_table_photo_video cannot be reverted.\n";

        return false;
    }
    */
}
