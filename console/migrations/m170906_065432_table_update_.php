<?php

use yii\db\Migration;

class m170906_065432_table_update_ extends Migration
{
    public function safeUp()
    {
      $sql = "alter table t_wx_apps change id id int not null auto_increment ";
      $this->execute($sql);
      $sql ="alter table t_wechat_user change id id int not null auto_increment ";
      $this->execute($sql);
    }

    public function safeDown()
    {
      
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170906_065432_table_update_ cannot be reverted.\n";

        return false;
    }
    */
}
