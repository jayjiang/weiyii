<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\libs\wxapi\WechatApi;
use common\models\WxApps;
class WxController extends  Controller {
   
    public function actionValid($id){
         $wx = WxApps::find()->where("id={$id}")->one();
         if(empty($wx)){
             echo "false";
             exit;
         }
         $wechat = new WechatApi();
         $wechat->setConfToken($wx->token);
         $wechat->valid();
         exit();
    }
    
    
}

