<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WxApps;

/**
 * WxAppsSearch represents the model behind the search form about `common\models\WxApps`.
 */
class WxAppsSearch extends WxApps
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type'], 'integer'],
            [['public_id', 'public_name', 'wechat', 'headface_url', 'token', 'appid', 'secret', 'encodingasekey', 'check_file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WxApps::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'public_id', $this->public_id])
            ->andFilterWhere(['like', 'public_name', $this->public_name])
            ->andFilterWhere(['like', 'wechat', $this->wechat])
            ->andFilterWhere(['like', 'headface_url', $this->headface_url])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'appid', $this->appid])
            ->andFilterWhere(['like', 'secret', $this->secret])
            ->andFilterWhere(['like', 'encodingasekey', $this->encodingasekey])
            ->andFilterWhere(['like', 'check_file', $this->check_file]);

        return $dataProvider;
    }
}
