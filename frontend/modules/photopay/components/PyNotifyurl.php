<?php
/**
 * Created by PhpStorm.
 * User: SFB
 * Date: 2017/9/8
 * Time: 12:28
 */

namespace frontend\modules\photopay\components;

use common\models\PhotoOrder;
use Yii;
use common\libs\payment\Notifyurl;

class PyNotifyurl extends Notifyurl
{

    //支付状态更改
    public  function update_status($out_trade_no,$trade_no,$trade_status,$pay_status)
    {
        $res = PhotoOrder::updateAll(['trade_no'=>$trade_no,'trade_status'=>$trade_status,'status'=>$pay_status=1],"order_no={$out_trade_no} and status=0");
        return $res>0;
    }

}