<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>看视频</title>
    <!-- zui -->
    <link href="/statics/photopay/css/zui.min.css" rel="stylesheet">
    <link href="/statics/photopay/css/zui-theme.css" rel="stylesheet">
    <script src="/statics/photopay/js/jquery-3.2.1.min.js"></script>
    <style>
        #main{
            width:100%;
            height:auto;
            margin:0 auto;
        }
        #main .pin{
            width:50%;
            height:auto;
            float:left;
        }
        #main .pin .box{
            width:100%;
            margin: 0 auto;
            padding:3px;
            border:1px solid #ddd;
            background:#fff;
            display: inline-block;
        }
    </style>
</head>
<body>
    
<!-- 在此处编码你的创意 -->

<script>
    $(function() {
        var pin = $('#main .pin');
        var mParent = $('#main');
        var baseWidth = pin[0].offsetWidth;
        mParent.css({
            width:baseWidth*2
        });
        var arrHeight = [];//高度的集合
        pin.each(function(i) {
            if(i<2) {
                arrHeight[i] = pin.eq(i).innerHeight();
            } else {
                //取最低一列的高度
                var minHeight = Math.min.apply({},arrHeight);
                //取最低一列的高度的元素的键名
                var key = getMin(arrHeight , minHeight);
                //取最低一列的left
                var left = pin.eq(key).position().left;
                //设置样式
                pin.eq(i).css({
                    position:'absolute',
                    top:minHeight,
                    left:left
                });
                arrHeight[key] += pin.eq(i).innerHeight();
            }
        });
        
        function getMin(arrHeight , min) {
            for(var i in arrHeight) {
                if(arrHeight[i] == min) {
                    return i;
                }
            }
        }
        
//        var clo = $('#main .pin').clone();
//        $('#main').html('');
//        for(var i=1 ; i<15 ; i++) {
//            $('#main').append(clo);
//        }
    });
</script>
<!-- 瀑布流 -->
<div id="main">
    <div class="pin">
        <div class="box">
            <a href=""><img src="/statics/photopay/images/5.jpg" /></a>
            <div class="card-actions">
            <div class="pull-right text-danger"><i class="icon-heart-empty"></i> 520 人喜欢</div>
            </div>
        </div>
    </div>
    <div class="pin">
        <div class="box">
            <a href=""><img src="/statics/photopay/images/2.jpg" /></a>
            <div class="card-actions">
            <div class="pull-right text-danger"><i class="icon-heart-empty"></i> 520 人喜欢</div>
            </div>
        </div>
    </div>
    <div class="pin">
        <div class="box">
            <a href=""><img src="/statics/photopay/images/3.jpg" /></a>
            <div class="card-actions">
            <div class="pull-right text-danger"><i class="icon-heart-empty"></i> 520 人喜欢</div>
            </div>
        </div>
    </div>
    <div class="pin">
        <div class="box">
            <a href=""><img src="/statics/photopay/images/2.jpg" /></a>
            <div class="card-actions">
            <div class="pull-right text-danger"><i class="icon-heart-empty"></i> 520 人喜欢</div>
            </div>
        </div>
    </div>
    <div class="pin">
        <div class="box">
            <a href=""><img src="/statics/photopay/images/5.jpg" /></a>
            <div class="card-actions">
            <div class="pull-right text-danger"><i class="icon-heart-empty"></i> 520 人喜欢</div>
            </div>
        </div>
    </div>
    <div class="pin">
        <div class="box">
            <a href=""><img src="/statics/photopay/images/3.jpg" /></a>
            <div class="card-actions">
            <div class="pull-right text-danger"><i class="icon-heart-empty"></i> 520 人喜欢</div>
            </div>
        </div>
    </div>
</div>


<!--<div class="messagers-holder bottom " style="text-align: center">
    <ul class="pager">
        <li><a href="#">个人中心</a></li>
        <li class="active"><a href="your/nice/url">2</a></li>
        <li><a href="your/nice/url">3</a></li>
        <li><a href="your/nice/url">4</a></li>
        <li><a href="your/nice/url">5</a></li>
    </ul>
</div>-->

<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<script src="/statics/photopay/js/jquery-3.2.1.min.js"></script>
<!-- ZUI Javascript组件 -->
<script src="/statics/photopay/js/zui.min.js"></script>
<script type="text/javascript">

</script>
</body>
</html>