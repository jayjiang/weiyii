<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\web\AssetBundle as AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <link rel="stylesheet" href="/statics/photopay/dist/lib/weui.min.css" >
    <link rel="stylesheet"  href="/statics/photopay/dist/css/jquery-weui.css"   >
    <link rel="stylesheet"  href="/statics/photopay/dist/css/my.css"   >
    <script  src="/statics/photopay/dist/lib/jquery-2.1.4.js"  ></script>
    <link rel="stylesheet" type="text/css" href="/statics/photopay/upload/webuploader.css">
    <script type="text/javascript" src="/statics/photopay/upload/webuploader.js"></script>
    <script type="text/javascript" src="/statics/photopay/upload/webuploader.min.js"></script>
    <script type="text/javascript" src="/statics/photopay/upload/upload3.js"></script>
</head>
<body>
<?php #$this->beginBody() ?>
  <!--<form id="upForm" enctype="multipart/form-data" method="post" action="<?php echo Yii::$app->urlManager->createUrl(['photopay/wap/upload/'.$wxid]) ?>">-->
  <?php $form = ActiveForm::begin([
	'id' => 'upForm',
	'action'=>Yii::$app->urlManager->createUrl(['photopay/wap/upload/'.$wxid]),
	'method' => 'post',
	'options' => ['enctype' => 'multipart/form-data']
	]); ?>
   <input type="file" name="upFile" id="upFile" accept="video/*"   style="display: none" />
   <input type="text" name="fileSize" id="fileSize"  style="display: none"  />
	
    <div class="weui-msg">
        <div class="weui-msg__icon-area" >
            <div id="uploader" class="wu-example">
                <div id="thelist" class="uploader-list"></div>
                <div  id="attach" >
                   <!-- <div > <img class="weui-media-box" src="./images/jh.png}"   /></div>-->
                </div>
                <div id="fileInfo" style="font-size: 8px"></div>
            </div>
        </div>
        <div class="weui-msg__text-area">
            <p class="weui-msg__desc">最大可以上传<span style="color:#f00;">50M</span>的视频文件</p>
        </div>
        <div class="weui-cell">
            <div class="weui-cell__hd"><label class="weui-label">标    题：</label></div>
            <div class="weui-cell__bd">
                <input class="weui-input" type="text" name="title" id="title"   placeholder="请输入文件标题">
            </div>
        </div>
        <div class="weui-cell">
            <div class="weui-cell__hd"><label class="weui-label">打    赏：</label></div>
            <div class="weui-cell__bd">
                <input class="weui-input" type="number"  onkeyup="inputNumberF(this,1)" id="fileMaxje" name="fileMaxje"  placeholder="请输入积分（1-50）">
            </div>
        </div>
        <div class='demos-content-padded'>

            <div class="weui-btn-area">
                <a href="javascript:;" class="weui-btn weui-btn_primary"  id="upload">上传</a>
            </div>
        </div>
    </div>
<!--</form>-->
<?php ActiveForm::end(); ?>



<script src="/statics/photopay/dist/lib/fastclick.js"  ></script>
<script src="/statics/photopay/dist/js/jquery-weui.js"   ></script>
<script>
    $(function() {
        FastClick.attach(document.body);
    });
    $('#slider1').slider(function (per) {
        console.log(per);
    });
</script>
<script src="/statics/photopay/copys/clipboard.js"></script>
<script src="/statics/photopay/copys/clipboard.min.js"></script>

<script>
    var clipboard = new Clipboard('.btn');

    clipboard.on('success', function(e) {
        $.alert("复制成功!!!");
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>
<?php # $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>