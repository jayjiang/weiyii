<?php
namespace frontend\modules\photopay\controllers;
use frontend\modules\photopay\components\WxController;
use Yii;
use common\libs\wxapi\WechatPay;
use common\models\WxApps;
use common\libs\wxapi\payapi\JsApiPay;
use common\libs\wxapi\payapi\base\WxPayUnifiedOrder;
/**
 *
 * @author SFB
 *        
 */
class PayController extends WxController
{
    public function actionIndex($id){
        if(isset($_REQUEST["uid"])&&isset($_REQUEST["oid"])&&isset($_REQUEST["totalFee"])&&isset($_REQUEST["orderName"])){
            //uid、oid
            $uid = $_REQUEST["uid"];
            $wxCofnig = WxApps::find()->where("id={$id}")->one();
            $oid = $_REQUEST["oid"];
            //微信支付参数
            $appid = $wxCofnig->appid;
            $mchid = $wxCofnig->mchid;
            $key = $wxCofnig->mchkey;
            $timestamp = time();
            $notifyUrl = Yii::$app->request->hostInfo.'/photopay/notify/'.$id;//$wxCofnig->notifyUrl;  //Yii::$app->params['wechat']['notifyUrl'];
            //商品订单参数
            $totalFee = $_REQUEST["totalFee"];
            $orderName = $_REQUEST["orderName"];
            //支付打包
            $wx_pay = new WechatPay($mchid, $appid, $key);
            $package = $wx_pay->createJsBizPackage($uid, $totalFee, $oid, $orderName, $notifyUrl, $timestamp);
            $result['error'] = 0;
            $result['msg'] = '支付打包成功';
            $result['package'] = $package;
        }else{
            $result['error'] = 1;
            $result['msg'] = '请求参数错误';
        }
        return $this->asJson($result);
    }
    
    public function actionConfig($id){
        if (isset($_REQUEST['url'])) {
            $url = $_REQUEST['url'];
            $wxCofnig = WxApps::find()->where("id={$id}")->one();
            //微信支付参数
            $appid = $wxCofnig->appid;
            $mchid = $wxCofnig->mchid;
            $key = $wxCofnig->mchkey;
            $wx_pay = new WechatPay($mchid, $appid, $key);
            $package = $wx_pay->getSignPackage($url);
            $result['error'] = 0;
            $result['msg'] = '获取成功';
            $result['config'] = $package;
        } else {
            $result['error'] = 1;
            $result['msg'] = '参数错误';
        }
        return $this->asJson($result);
    }

    public function actionPay($id){
        $wxCofnig = WxApps::find()->where("id={$id}")->one();


        //①、获取用户openid
        //$appid,$mchid,$mchkey,$url,$SSLCERT_PATH,$SSLKEY_PATH
        $url = Yii::$app->request->hostInfo.'/photopay/notify/'.$id;
        $tools = new JsApiPay($wxCofnig->appid,$wxCofnig->mchid,$wxCofnig->mchkey,$url,$wxCofnig->cert_pem,$wxCofnig->key_pem);
        $tools->setAppSecret($wxCofnig->secret);
        $openId = $this->openid;

        //②、统一下单
        $input = new WxPayUnifiedOrder();
        $input->SetBody("test");
        $input->SetAttach("test");
        //$input->SetOpenid($openId);
        $input->SetAppid($wxCofnig->appid);
        $input->SetMch_id($wxCofnig->mchid);
        $input->setMchkey($wxCofnig->mchkey);
        $input->SetOut_trade_no($wxCofnig->mchid.date("YmdHis"));
        $input->SetTotal_fee("1");
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag("test");
        $input->SetNotify_url( Yii::$app->request->hostInfo.'/photopay/notify/'.$id);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $order = $tools->unifiedOrder($input);
        var_dump($order);exit;
        echo '<font color="#f00"><b>统一下单支付单信息</b></font><br/>';
        print_r($order);
        $jsApiParameters = $tools->GetJsApiParameters($order);

       //获取共享收货地址js函数参数
        $editAddress = $tools->GetEditAddressParameters();
        return $this->render("pay",['jsApiParameters'=>$jsApiParameters,'editAddress'=>$editAddress]);
    }

    public function actionPay1($id){
        //多公众号使用方式
//        $wechat = Yii::createObject([
//            'class' => 'callmez\wechat\sdk\Wechat',
//            'appId' => '微信公众平台中的appid',
//            'appSecret' => '微信公众平台中的secret',
//            'token' => '微信服务器对接您的服务器验证token'
//        ]);

        if($this->openid)
        {

            $order_=[
                'goods_desc'=>'测试',
                'order_sn' =>'2222'.time(),
                'total_fee' =>0.1,
                'body'=>'测试',
                'time_start' =>date("YmdHis"),
                'time_expire'=>date("YmdHis", time() + 86400*300),
                'goods_tag' =>'',
                'notify_url'=>Yii::$app->request->hostInfo.'/photopay/notify/'.$id,//这是回调地址，微信通知的地址
                'openid'=>$this->openid,
            ];
            $wxCofnig = WxApps::find()->where("id={$id}")->one();
            $payment =Yii::createObject([
                'class'=>'common\libs\payment\Instance',

                'weixinjspi_config' => [
                    //'code'      => 2,
                    'appid'     => $wxCofnig->appid,//微信的appid
                    'secret'    => $wxCofnig->secret,//appsecret，在申请完公众号以后可以看到
                    'mch_id'    => $wxCofnig->mchid,//商户号
                    'key'       => $wxCofnig->mchkey,//key需要设置
                    'cert_path' => $wxCofnig->cert_pem,//可以不用填写
                    'key_path'  => $wxCofnig->key_pem,//可以不用填写
                ],
            ]);
            $paymodel=$payment->getWeixinjspi();

            $result=$paymodel->pay($order_);//生成预付单
            if($result)
            {
                $jsstr=$this->getJs($result,$order_['order_sn']);
                //根据预付单信息生成js，详细的可以看上面的类的方法。

            }


        }


       return $this->render("pay1",['jsstr'=>$jsstr]);

       // echo $jsstr;

    }
}

