<?php

namespace frontend\modules\photopay\controllers;
use Yii;
use yii\web\Controller;
use frontend\modules\photopay\components\BaseController;
use common\models\WxApps;

/**
 * Default controller for the `photopay` module
 */
class DefaultController extends BaseController

{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
	  $model = WxApps::findOne($id);
	  
      return $this->render('index',['model'=>$model]);
    }
}
