<?php
/**
 * Created by PhpStorm.
 * User: SFB
 * Date: 2017/9/4
 * Time: 8:56
 */

namespace frontend\modules\photopay\controllers;

use common\models\PhotoOrder;
use common\models\PhotoVideo;
use Yii;
use frontend\modules\photopay\components\WxController;
use yii\web\UploadedFile;
use FFMpeg\FFMpeg;
use common\models\WxApps;
use frontend\modules\photopay\components\PyNotifyurl;
class WapController extends  WxController
{

  //public function 
  
    public function  actionIndex(){
        return $this->render("index");
    }

    /**
     * 上传视频
     */
    public function actionUpload($id){
        
        if(Yii::$app->request->isPost){
            $title = Yii::$app->request->post("title");
            $type = Yii::$app->request->post("type");
			$model = new WxApps();
            if(!empty($_FILES)){
                $file = UploadedFile::getInstanceByName("file");
                $time = time();
                $rand = rand(1000,9999);
                $or_filepath =  '/uploads/temp/' . $file->baseName .$time. $rand.'.' . $file->extension;
                $file->saveAs(APP_PATH.$or_filepath);
                $mpeg =  FFMpeg::create(array(
                    'ffmpeg.binaries'  => Yii::$app->params['ffmpeg']['ffmpeg'],//'D:/ffmpeg/bin/ffmpeg.exe',
                    'ffprobe.binaries' => Yii::$app->params['ffmpeg']['ffprobe'],// 'D:/ffmpeg/bin/ffprobe.exe',
                    'timeout'          => 3600, // The timeout for the underlying process
                    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
                ));
                $extension = $file->extension;
                $video_path = '/uploads/videos/' . $file->baseName .$time.$rand. '.'.$extension;
				copy(APP_PATH.$or_filepath,APP_PATH.$video_path);
                $video = $mpeg->open(APP_PATH.$or_filepath);
                $video = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(1));
                $imgFileName = '/uploads/images/'.$time.'-'.$rand.'.png';
                $video->save(APP_PATH.'/'.$imgFileName);
                if(is_file(APP_PATH.$or_filepath)){
                    unlink(APP_PATH.$or_filepath);
                }
				$model->title = $title;
				$model->opneid = $this->openid;
				$model->wxid = $id;
				$model->type=0;
				$model->video_img = $imgFileName;
				$model->video_src = $video_path;
				$model->status = 1;
				if($model->save()){
				  return $this->redirect(['/photopay/wap/pay/'.$id,'vid'=>$model->id]);
				}
				
            }
        }
        return $this->render("upload",['wxid'=>$id]);
    }
	
	
    
    /**
     * 调微信支付接口
     */
    public function acitonPay($id){
        if($this->openid){
            $videoId = Yii::$app->request->get('vid');
            $wxCofnig = WxApps::find()->where("id={$id}")->one();
            $video = PhotoVideo::find()->where("id=".$videoId)->once();
            $money = round(1,50);
            $order_no = "PY".time().$money;
            $orderModel = new PhotoOrder();
            $orderModel->opneid=$this->openid;
            $orderModel->wxid = $id;
            $orderModel->video_id = $video->id;
            $orderModel->status = 0;
            $orderModel->money = $money;
            if($orderModel->save()){
			  $order_=[
				  'goods_desc'=>'打赏看视频',
				  'order_sn' =>$order_no,
				  'total_fee' =>$money,
				  'body'=>'打赏看视频',
				  'time_start' =>date("YmdHis"),
				  'time_expire'=>date("YmdHis", time() + 86400*300),
				  'goods_tag' =>'',
				  'notify_url'=>Yii::$app->request->hostInfo.'/photopay/notify/'.$id,//这是回调地址，微信通知的地址
				  'openid'=>$video->openid,
			  ];

			  $payment =Yii::createObject([
				  'class'=>'common\libs\payment\Instance',

				  'weixinjspi_config' => [
					  //'code'      => 2,
					  'appid'     => $wxCofnig->appid,//微信的appid
					  'secret'    => $wxCofnig->secret,//appsecret，在申请完公众号以后可以看到
					  'mch_id'    => $wxCofnig->mchid,//商户号
					  'key'       => $wxCofnig->mchkey,//key需要设置
					  'cert_path' => $wxCofnig->cert_pem,//可以不用填写
					  'key_path'  => $wxCofnig->key_pem,//可以不用填写
				  ],
			  ]);
			  $paymodel=$payment->getWeixinjspi();

			  $result=$paymodel->pay($order_);//生成预付单
			  if($result)
			  {
				  $jsstr=$this->getJs($result,$order_['order_sn']);
				  //根据预付单信息生成js，详细的可以看上面的类的方法。
			  }
			  return $this->render("pay",['jsstr'=>$jsstr,'video'=>$video]);
			}
       }
	   

    }
	
	
	/**
	 *  支付回调
	 * @param type $id
	 */
	public function actionNotify($id){
	  $notify = new  PyNotifyurl();
	  $wxCofnig = WxApps::find()->where("id={$id}")->one();
	  $payment =Yii::createObject([
			   'class'=>'common\libs\payment\Instance',

			   'weixinjspi_config' => [
				   //'code'      => 2,
				   'appid'     => $wxCofnig->appid,//微信的appid
				   'secret'    => $wxCofnig->secret,//appsecret，在申请完公众号以后可以看到
				   'mch_id'    => $wxCofnig->mchid,//商户号
				   'key'       => $wxCofnig->mchkey,//key需要设置
				   'cert_path' => $wxCofnig->cert_pem,//可以不用填写
				   'key_path'  => $wxCofnig->key_pem,//可以不用填写
			   ],
		   ]);
	   $paymodel=$payment->getWeixinjspi();
	   $notify->notify($paymodel);
	}
	
	public function acitonOrder(){
	  $oid = Yii::$app->request->get('oid');
	  $order = PhotoOrder::findOne($oid);
	  $video = PhotoVideo::findOne($order->video_id);
	  $wxUser = \common\models\WechatUser::find()->where(['opneid'=>$video->opneid,'wx_id'=>$video->wxid])->one();
	  
	}

}